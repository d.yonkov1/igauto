const path = require('path');
const { saveImage } = require('./download');
const { readFile, rmDir, mkDir } = require("./utils");

async function download() {
    let data = await readFile(path.join(__dirname, "./posts.json"));
    data = JSON.parse(data);

    const keys = Object.keys(data);

    console.log(`Downloading ${keys.length} posts`);

    await rmDir(path.join(__dirname, "./database/"))
    await mkDir(path.join(__dirname, "./database/"))

    for (const key of keys) {
        console.log(`Start saving file with key: ${key}`)

        for (let index = 0; index < data[key].url.length; index++) {
            const url = data[key].url[index];

            await saveImage(key, url, index)
        }
    }
}

module.exports = {
    download
}