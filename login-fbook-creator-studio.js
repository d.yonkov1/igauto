const puppeteer = require("puppeteer-extra");
const sleep = require("util").promisify(setTimeout);

const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

async function login() {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-web-security',
      '--user-data-dir=./chrome-session']
  });
  const page = await browser.newPage();

  await page.goto("https://business.facebook.com/creatorstudio/home");
};

module.exports = {
  login
}