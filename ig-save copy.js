const path = require("path");
const puppeteer = require("puppeteer-extra");

const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

const { writeFile } = require("./utils");

// const codes = [
//   "CZ6CCApqDzd"
// ];

const data = [];

async function save(codes) {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      '--user-data-dir=./chrome-session']
  });


  return Promise.all(codes.map(async code => {
    const singleData = await getPostData(browser, code);
    data.push(singleData);
  }))
    .then(async () => {
      await writeFile(path.join(__dirname, "./posts.json"), JSON.stringify(data))

      browser.close();
    });
}

async function getPostData(browser, shortCode) {
  console.error(`Getting data for ${shortCode}`);
  const page = await browser.newPage();
  await page.goto(`https://www.instagram.com/p/${shortCode}/?__a=1`);

  // fail safe
  await page.content();

  const data = await page.evaluate(() => {
    return JSON.parse(document.querySelector("body").innerText);
  });

  const caption = data.items[0].caption?.text;
  const username = data.items[0].user?.username;

  let url = [];
  if (data.items[0].carousel_media) {
    url = data.items[0].carousel_media.map(item => item.image_versions2.candidates[0].url);
  } else {
    url = [data.items[0].image_versions2.candidates[0].url];
  }

  return {
    shortCode,
    url,
    caption,
    username
  }
}

module.exports = {
  save
}