import { createApp } from 'vue'

import App from './App.vue'
import PrimeVue from 'primevue/config';
import Button from 'primevue/button';
import TextArea from 'primevue/textarea';
import Calendar from 'primevue/calendar';
import InputText from 'primevue/inputtext';
import Dropdown from 'primevue/dropdown'
import Checkbox from 'primevue/checkbox'
import Tooltip from 'primevue/tooltip';

import 'primeicons/primeicons.css'
import 'primevue/resources/themes/tailwind-light/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'

import './css/index.css'

const app = createApp(App);
app.use(PrimeVue);
app.component('Button', Button);
app.component('TextArea', TextArea);
app.component('Calendar', Calendar);
app.component('InputText', InputText);
app.component('Dropdown', Dropdown);
app.component('Checkbox', Checkbox);

app.directive('tooltip', Tooltip);

app.mount('#app');
