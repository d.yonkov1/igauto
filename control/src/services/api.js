const API_URL = `http://localhost:3000/api/v1`;

async function loginToInstagram() {
    try {
        const response = await fetch(`${API_URL}/instagram/login`, {
            method: "POST"
        }).catch(error => {
            console.error(error);
        })

        console.error(await response.json());
        return response.json()

    } catch (error) {
    }
}

async function loginToCreatorStudio() {
    const response = await fetch(`${API_URL}/creator_studio/login`, {
        method: "POST"
    })

    return response.json()

}

async function post(data) {
    const response = await fetch(`${API_URL}/instagram/post`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
        }
    })

    return response.json()
}

export {
    loginToInstagram,
    loginToCreatorStudio,
    post
}