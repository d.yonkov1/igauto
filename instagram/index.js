const puppeteer = require("puppeteer-extra");
const sleep = require("util").promisify(setTimeout);
const fs = require("fs");
const path = require("path");
const clipboardy = require('clipboardy');

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

const config = JSON.parse(fs.readFileSync(path.resolve(__dirname, "./config.json")));

async function schedulePosts(posts = [], onDone) {
    const browser = await puppeteer.launch({
        headless: false,
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-web-security',
            '--user-data-dir=./chrome-session']
    });
    const page = await browser.newPage();

    // Configure the navigation timeout
    await page.setDefaultNavigationTimeout(0);
    await page.goto(config.loginURL)

    // return
    for (let post of posts) {
        try {
            await page.reload({ waitUntil: "networkidle2" });

            // await clickElementByAccName(page, "Create post");

            for (let index = 0; index < 5; index++) {
                page.keyboard.press('Tab');
            }

            page.keyboard.press('Space');
            page.keyboard.press('Space');

            // Add description
            await page.waitForSelector(config.selectors.descriptionInput);

            let descriptionInput = await page.$(
                config.selectors.descriptionInput
            );

            await descriptionInput.type("Credits: ", {
                delay: config.delays.typeDelay,
            });

            clipboardy.writeSync(post.description);

            page.keyboard.down('Control');
            page.keyboard.press('V');
            page.keyboard.up('Control');

            page.keyboard.press('Tab');
            page.keyboard.press('Tab');
            page.keyboard.press('Tab');

            page.keyboard.press('Space');

            await page.waitForSelector(config.selectors.imageInput);

            let imageInput = await page.$(config.selectors.imageInput);
            await imageInput.uploadFile(...post.file);

            await sleep(1500);

            // Schedule Post
            await clickElementByAccName(page, "dropdownButton");
            await clickElementByAccName(page, "dropdown menu item");

            // Add Release Date
            let releaseDateInput = await page.$(
                config.selectors.releaseDateInput
            );
            await releaseDateInput.type(post.release.date, {
                delay: config.delays.typeDelay,
            });

            // Add Release Time
            let releaseTime = post.release.time.split(":");

            let releaseTimeInput = await page.$$(
                config.selectors.releaseTimeInput
            );
            let hourInput = releaseTimeInput[0];
            let minuteInput = releaseTimeInput[1];

            await hourInput.type(releaseTime[0], {
                delay: config.delays.typeDelay,
            });
            await page.waitForTimeout(100);
            await minuteInput.type(releaseTime[1], {
                delay: config.delays.typeDelay,
            });

            // Publish
            await clickElementByAccName(page, "Schedule");

            onDone && onDone(post)

            await sleep(config.delays.postDelay);

        } catch (error) {
            continue;
        }
    }

    browser.close();
}

async function clickElementByAccName(page, accName) {
    const frame = page.mainFrame();
    const element = await frame.waitForSelector(`aria/${accName}`);
    await element.click();
}

module.exports = {
    schedulePosts
};