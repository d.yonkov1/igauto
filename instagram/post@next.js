const puppeteer = require("puppeteer-extra");
const sleep = require("util").promisify(setTimeout);
const fs = require("fs");
const path = require("path");
const clipboardy = require('clipboardy');
const atob = require('atob');
const btoa = require('btoa');
const prettier = require('prettier');

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

const config = JSON.parse(fs.readFileSync(path.resolve(__dirname, "./configNext.json")));

function getNewBody(oldBody) {
    return `
    document.createElement = function (create) {
        window.inputs = []
        return function () {
          var ret = create.apply(this, arguments);
          if (arguments[0] === "input") {
            console.error("bomba", ret)
            window.inputs.push(ret)
          }
          return ret;
      };
    }(document.createElement);
    ${oldBody}`;
}

const scriptUrlPatterns = [
    '*'
]

let intercepted = false;
const requestCache = new Map();

async function interceptRequestsForPage(page) {
    const client = await page.target().createCDPSession();


    await client.send("Page.enable")
    await client.send('Network.enable');

    await client.send('Network.setRequestInterception', {
        patterns: scriptUrlPatterns.map(pattern => ({
            urlPattern: pattern, resourceType: 'Script', interceptionStage: 'HeadersReceived'
        }))
    });

    client.on('Network.requestIntercepted', async ({ interceptionId, request, responseHeaders, resourceType }) => {
        const response = await client.send('Network.getResponseBodyForInterception', { interceptionId });

        const contentTypeHeader = Object.keys(responseHeaders).find(k => k.toLowerCase() === 'content-type');
        let newBody, contentType = responseHeaders[contentTypeHeader];

        if (requestCache.has(response.body)) {
            newBody = requestCache.get(response.body);
        } else {

            if (intercepted || ['text/javascript', 'application/x-javascript; charset=utf-8'].indexOf(contentType) === -1) {
                client.send('Network.continueInterceptedRequest', { interceptionId });
                return
            }

            const bodyData = response.base64Encoded ? atob(response.body) : response.body;
            try {
                if (resourceType === 'Script') newBody = prettier.format(getNewBody(bodyData), { parser: 'babel' })
                else newBody === getNewBody(bodyData)
            } catch (e) {
                console.log(`Failed to process ${request.url} {interception id: ${interceptionId}}: ${e}`);
                newBody = bodyData
            }

            requestCache.set(response.body, newBody);
        }

        const newHeaders = [
            'Date: ' + (new Date()).toUTCString(),
            'Connection: closed',
            'Content-Length: ' + newBody.length,
            'Content-Type: ' + contentType
        ];

        console.log(`Intercepted ${request.url} {interception id: ${interceptionId}}`);
        console.log(`Continuing interception ${interceptionId}`)
        client.send('Network.continueInterceptedRequest', {
            interceptionId,
            rawResponse: btoa('HTTP/1.1 200 OK' + '\r\n' + newHeaders.join('\r\n') + '\r\n\r\n' + newBody)
        });
        intercepted = true;
    });
}

async function schedulePosts(posts = [], onDone) {
    const browser = await puppeteer.launch({
        headless: false,
        devtools: true,
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-web-security',
            '--user-data-dir=./chrome-session']
    });
    // const page = await browser.newPage();
    const page = (await browser.pages())[0];

    // Configure the navigation timeout
    await page.setDefaultNavigationTimeout(0);

    await interceptRequestsForPage(page);

    for (let post of posts) {
        try {
            // await page.reload({ waitUntil: "networkidle2" });
            await page.goto(config.loginURL)

            await clickElementByAccName(page, 'Cancel');

            // Add description
            await page.waitForSelector(`aria/Write into the dialogue box to include text with your post.`);

            let descriptionInput = await page.$(
                `aria/Write into the dialogue box to include text with your post.`
            );

            await descriptionInput.type("Credits: ", {
                delay: config.delays.typeDelay,
            });

            clipboardy.writeSync(post.description);

            page.keyboard.down('Control');
            page.keyboard.press('V');
            page.keyboard.up('Control');

            const input = await page.evaluate(() => {
                return window.inputs.find(i => i.type === 'file');
            });

            await input.uploadFile(...post.file);
        } catch (e) {
            console.error(e);
            continue;
        }
    }
    await page.goto(config.loginURL)

    await clickElementByAccName(page, 'Cancel');

    // Add description
    await page.waitForSelector(`aria/Write into the dialogue box to include text with your post.`);

    let descriptionInput = await page.$(
        `aria/Write into the dialogue box to include text with your post.`
    );

    await descriptionInput.type("Credits: ", {
        delay: config.delays.typeDelay,
    });

    clipboardy.writeSync(post.description);

    page.keyboard.down('Control');
    page.keyboard.press('V');
    page.keyboard.up('Control');

    const input = await page.evaluate(() => {
        return window.inputs.find(i => i.type === 'file');
    });

    await input.uploadFile(...post.file);

    await sleep(50000);

    // // return
    // for (let post of posts) {
    //     try {
    //         await page.reload({ waitUntil: "networkidle2" });

    //         await clickElementByAccName(page, "Create post");

    //         page.keyboard.press('Space');

    //         // Add description
    //         await page.waitForSelector(config.selectors.descriptionInput);

    //         let descriptionInput = await page.$(
    //             `aria/Write into the dialogue box to include text with your post.`
    //         );

    //         await descriptionInput.type("Credits: ", {
    //             delay: config.delays.typeDelay,
    //         });

    //         clipboardy.writeSync(post.description);

    //         page.keyboard.down('Control');
    //         page.keyboard.press('V');
    //         page.keyboard.up('Control');

    //         page.keyboard.press('Tab');
    //         page.keyboard.press('Tab');
    //         page.keyboard.press('Tab');

    //         page.keyboard.press('Space');

    //         await page.waitForSelector(config.selectors.imageInput);

    //         let imageInput = await page.$(config.selectors.imageInput);
    //         await imageInput.uploadFile(...post.file);

    //         await sleep(500);

    //         // Schedule Post
    //         await clickElementByAccName(page, "dropdownButton");
    //         await clickElementByAccName(page, "dropdown menu item");

    //         // Add Release Date
    //         let releaseDateInput = await page.$(
    //             config.selectors.releaseDateInput
    //         );
    //         await releaseDateInput.type(post.release.date, {
    //             delay: config.delays.typeDelay,
    //         });

    //         // Add Release Time
    //         let releaseTime = post.release.time.split(":");

    //         let releaseTimeInput = await page.$$(
    //             config.selectors.releaseTimeInput
    //         );
    //         let hourInput = releaseTimeInput[0];
    //         let minuteInput = releaseTimeInput[1];

    //         await hourInput.type(releaseTime[0], {
    //             delay: config.delays.typeDelay,
    //         });
    //         await page.waitForTimeout(100);
    //         await minuteInput.type(releaseTime[1], {
    //             delay: config.delays.typeDelay,
    //         });

    //         // Publish
    //         await clickElementByAccName(page, "Schedule");

    //         onDone && onDone(post)

    //         await sleep(config.delays.postDelay);

    //     } catch (error) {
    //         continue;
    //     }
    // }

    // browser.close();
}

async function clickElementByAccName(page, accName) {
    const frame = page.mainFrame();
    const element = await frame.waitForSelector(`aria/${accName}`);
    await element.click();
}

module.exports = {
    schedulePosts
};