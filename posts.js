const fs = require("fs");
const fsPromises = fs.promises;
const path = require("path");
const { schedulePosts } = require("./instagram");
const { writeFile, rmDir, shuffle } = require("./utils");

// puppeteer-extra is a drop-in replacement for puppeteer,
// it augments the installed puppeteer with plugin functionality
const puppeteer = require('puppeteer-extra')

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

let state = {}
let data = []
let hashtags = []

const FORCE_AUTHOR_CREDIT = false;

function getNextData() {
    let date = state.date;

    if (!date) {
        date = new Date();
    } else {
        date = new Date(date);
    }

    state.currentHour = (state.currentHour + 1) >= state.hours.length ? 0 : state.currentHour + 1

    const hour = state.hours[state.currentHour]

    if ((state.currentHour + 1) >= state.hours.length) {
        date.setDate(date.getDate() + 1);
        state.date = date = date.toLocaleString()
    }

    date = date.toLocaleString()
    const d = date.split(".")[0].split("/");

    return {
        date: `${d[0]}/${d[1]}/${d[2]}`,
        time: hour
    }
}

const getDirectories = async (source, shuffled = false) => {
    let content = await fsPromises.readdir(source, { withFileTypes: true })

    content = content.filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name)

    return shuffled ? shuffle(content) : content;
}

const getFiles = async source => {
    const content = await fsPromises.readdir(source, { withFileTypes: true })

    return content
        .map(dirent => dirent.name)
}

function getHashtags(caption, forceHashtags) {
    if (forceHashtags) {
        return hashtags.slice(0, 30).join(" ")
    }
    return caption.filter(el => el.startsWith("#")).concat(hashtags).slice(0, 30).join(" ")
}

function getCredits(caption, author, creditAuthor) {
    // extract mentions
    let mentions = caption.filter(el => el.startsWith("@") && el !== `@${author}`)

    // remove duplicate mentions
    mentions = [...new Set(mentions)];

    // if FORCE_AUTHOR_CREDIT=true - credit the author for sure
    if (creditAuthor || FORCE_AUTHOR_CREDIT) {
        mentions.unshift(`@${author}`)
    }

    // if no mentions - credit the author at least
    return mentions.length ? mentions.join(" ") : "@ DM??";
}

async function getPosts(creditAuthor, forceHashtags) {
    const ids = await getDirectories(path.resolve(__dirname, "./database"));

    return Promise.all(ids.map(async id => {
        if (!data[id]) {
            return
        }
        console.log(`Generating post with ID: ${id}`)

        const username = data[id].username;
        const release = getNextData();

        // split caption by space and new line
        let caption = data[id].caption.split(/\s+/);

        let credits = getCredits(caption, username, creditAuthor);
        let hashtags = getHashtags(caption, forceHashtags);

        const files = await getFiles(path.resolve(__dirname, `./database/${id}`));

        return {
            id,
            description: `${credits}
                    .
                    .
                    .
                    .
                    .
                    ${hashtags}`,
            file: files.map((f, i) => `./database/${id}/image-${i}.png`),
            release
        }
    }))
}

async function init(options) {
    const { date, hours, hashtags: rawHashtags, creditAuthor, forceHashtags } = options;

    data = JSON.parse(fs.readFileSync(path.resolve(__dirname, "./posts.json")))

    state = {
        currentHour: 0,
        date,
        hours
    };

    hashtags = rawHashtags;

    const posts = await getPosts(creditAuthor, forceHashtags);

    schedulePosts(posts, ({ id }) => {
        rmDir(path.join(__dirname, `./database/${id}`))
    });
}

module.exports = {
    post: init
}