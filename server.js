const express = require('express')
const cors = require('cors')
const logger = require("morgan");

const { login: IGLogin } = require('./ig-login')
const { login: CSLogin } = require('./login-fbook-creator-studio')
const { post } = require('./posts')
const { download } = require('./download-posts')
const { save } = require('./ig-save')

const app = express()
const port = 3000

const apiUrl = '/api/v1'

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("./control/dist"));

app.use(cors())

app.post(`${apiUrl}/instagram/login`, async (req, res, next) => {
    try {
        await IGLogin()

        res.send({
            message: 'Successfully logged in to Instagram'
        })
    } catch (error) {
        next(error)
    }
})

app.post(`${apiUrl}/creator_studio/login`, async (req, res, next) => {
    try {
        await CSLogin()

        res.send({
            message: 'Successfully logged in to Creator Studio'
        })
    } catch (error) {
        next(error)
    }
})

// instagram/post
app.post(`${apiUrl}/instagram/post`, async (req, res, next) => {
    console.error(req.body)
    try {
        const {
            date,
            hours,
            codes,
            hashtags,
            creditAuthor,
            forceHashtags
        } = req.body

        await save(codes)
        await download()
        await post({ date, hours, hashtags, creditAuthor, forceHashtags })

        res.send({
            message: 'Successfully posted to Instagram',
        })
    } catch (error) {
        next(error)
    }
})

function errorHandler(err, req, res, next) {
    if (req.xhr) {
        res.status(500).send({ error: 'Something failed!' })
    } else {
        next(err)
    }
}

app.use(errorHandler)

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})