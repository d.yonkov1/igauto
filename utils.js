const fs = require("fs").promises;

const writeFile = (path, content) => {
    try {
        return fs.writeFile(path, content);
    } catch (error) {
        console.error(`Got an error trying to write the file: ${error.message}`);
    }
};

const readFile = (path, encoding = 'utf-8') => {
    try {
        return fs.readFile(path, encoding);
    } catch (error) {
        console.error(`Got an error trying to read the file: ${error.message}`);
    }
}

const rmDir = async (path) => {
    try {
        await fs.access(path);

        return fs.rmdir(path, {
            recursive: true
        })
    } catch (error) {
        console.error(`Got an error trying to remove a directory: ${error.message}`);
    }
}

const mkDir = async (path) => {
    try {        
        return fs.mkdir(path, {
            recursive: true
        })
    } catch (error) {
        console.error(`Got an error trying to make a directory: ${error.message}`);
    }
}

function shuffle(array) {
    var currentIndex = array.length,  randomIndex;
  
    // While there remain elements to shuffle...
    while (currentIndex != 0) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
  
    return array;
  }

module.exports = {
    writeFile,
    readFile,
    rmDir,
    mkDir,
    shuffle
}