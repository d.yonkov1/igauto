# Instagram automation - repost and schedule

## Project setup
```
git clone https://gitlab.com/d.yonkov1/igauto.git
```
```
cd igauto
```
```
npm install
```

### Start
```
npm run start
```

### Open
```
http://localhost:3000/index.html
```

## Demo
![](https://gitlab.com/d.yonkov1/igauto/-/raw/main/demo.png)
