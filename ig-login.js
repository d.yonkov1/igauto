const puppeteer = require("puppeteer-extra");

const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

async function login() {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      '--user-data-dir=./chrome-session']
  });

  const page = await browser.newPage()

  await page.goto("https://www.instagram.com/")
};

module.exports = {
  login
}