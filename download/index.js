const fs = require("fs").promises;
const path = require("path");
const { writeFile } = require("../utils");

// puppeteer-extra is a drop-in replacement for puppeteer,
// it augments the installed puppeteer with plugin functionality
const puppeteer = require('puppeteer-extra')

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

async function saveImage(id, url, index = 0) {
    const browser = await puppeteer.launch({
        headless: true, args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
        ],
    });
    const page = await browser.newPage();

    // Configure the navigation timeout
    await page.setDefaultNavigationTimeout(0);
    await page.goto(url)

    try {
        await fs.access(path.resolve(__dirname, `../database/${id}`));
        // The check succeeded
    } catch (error) {
        // The check failed
        await fs.mkdir(path.resolve(__dirname, `../database/${id}`));
    }

    const viewSource = await page.goto(url);
    const localImagePath = `../database/${id}/image-${index}.png`

    await writeFile(path.resolve(__dirname, localImagePath), await viewSource.buffer())
    browser.close();
}

module.exports = {
    saveImage
}