const path = require("path");
const puppeteer = require("puppeteer-extra");

const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

const { writeFile } = require("./utils");

const codes_data = [
  "CfaSIzvLFwv",
  "CfWgfA1Lbmz",
  "CfTsf27rERr",
  "CfJ5VgzrWWI",
  "CfNChB-v15f",
  "CfH2BQfPmkP",
];


async function save(codes) {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      '--user-data-dir=./chrome-session']
  });

  const data = [];

  return Promise.all(codes.map(async code => {
    let singleData = await getPostData(browser, code);
    console.error(singleData);
    if (!singleData) {
      singleData = await getPostData2(browser, code)
    }

    if (singleData) {
      data.push(singleData);
    }
  }))
    .then(async () => {
      await writeFile(path.join(__dirname, "./posts.json"), JSON.stringify(data))

      browser.close();
    });
}

function getPostData2(browser, shortCode) {
  return new Promise(async (resolve, reject) => {
    console.error(`Getting data for ${shortCode}`);
    const page = await browser.newPage();

    page.on('response', async (response) => {
      if (response.url().endsWith("info/") && response.url().startsWith('https://i.instagram.com/api/v1/media')) {
        let data = {};
        let status;

        if (response.status) {
          status = response.status();
        }
        if (
          status // we actually have an status for the response
          && !(status > 299 && status < 400) // not a redirect
          && !(status === 204) // not a no-content response
        ) {
          try {
            data = await response.json();

            console.error(data);
            if (data.status !== "ok") {
              return resolve(undefined);
            }

            const item = data.items[0]

            // caption
            const caption = item.caption.text;

            // username
            const username = item.user.username;

            let url = [];

            if (item.image_versions2) {

              const images = item.image_versions2.candidates;

              url = [new URL(images[0].url).href];

            } else if (item.carousel_media) {
              url = item.carousel_media.map(m => new URL(m.image_versions2.candidates[0].url).href)
            }

            resolve({
              shortCode,
              url,
              caption,
              username
            });
          } catch (error) {
            console.error(error)
          }
        }
      }

    });

    await page.goto(`https://www.instagram.com/p/${shortCode}`);

  });

}

function getPostData(browser, shortCode) {
  return new Promise(async (resolve, reject) => {
    console.error(`Getting data for ${shortCode}`);
    const page = await browser.newPage();

    page.on('response', async (response) => {
      if (response.url().startsWith('https://www.instagram.com/graphql/query/?query_hash=')) {
        console.log('XHR response received');
        let data = {};
        let status;

        if (response.status) {
          status = response.status();
        }
        if (
          status // we actually have an status for the response
          && !(status > 299 && status < 400) // not a redirect
          && !(status === 204) // not a no-content response
        ) {
          try {
            data = await response.json();
          } catch (error) {
            console.error(error)
          }
        }

        const edges = data?.data?.user?.edge_owner_to_timeline_media?.edges

        if (edges) {
          console.error(shortCode);
          let edge = edges.find(e => e.node.shortcode === shortCode);

          if (!edge || !edge.node) {
            return resolve(undefined)
          }

          edge = edge.node

          // urls
          let url = [];
          if (edge.edge_sidecar_to_children) {
            url = edge.edge_sidecar_to_children.edges.map(e => e.node.display_url)
          } else {
            url = [edge.display_url]
          }

          // caption
          const caption = edge.edge_media_to_caption.edges[0].node.text;

          // username
          const username = edge.owner.username;

          resolve({
            shortCode,
            url,
            caption,
            username
          });

        }
      }
    });

    await page.goto(`https://www.instagram.com/p/${shortCode}`);
  })
}

module.exports = {
  save
}